package z123;


import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class test {

    private static Logger logger = LogManager.getLogger("Hello World");

    public static void main(String[] args) {
        logger.info("Hello, world");
        Logger fileLog = LogManager.getLogger("task11.xml");
        fileLog.debug("Write in file!");
    }
}
